# Learning Actors - OTE: Java 9 Modules Example Project

This is a sample project containing a concept transition from a legacy simple app, towards Java Modules

## Branches
There are three branches
* master
* 1-java-before-java9-modules-example
* 2-after-java-9-modules

The example is about differences between branches `-java-before-java9-modules-example` and `2-after-java-9-modules`

In order to execute, just run the MainExample's main method.

### Requirements Note
In order to run the `master` & `1-java-before-java9-modules-example` branches using the JDBC driver, one needs to add
the jar contained java9-modules folder:
![img_4.png](img_4.png)

and submit onto the next prompt:

![img_5.png](img_5.png)


Additionally, this example requires a running MySql server instance on the host environment, as well as 
changing the connection credentials accordingly into the example classes.
MySQL version used in this example was 8.2.0, compatible with the provided jar connector

We can run one MySql Docker container instance by:
> docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=admin123 -p 3306:3306 -d mysql:latest

and then connect to it either from Intellij by adding a Data Source:
![img_3.png](img_3.png)

![img_2.png](img_2.png)


or by executing the following docker command to get a MySQL shell:

> docker exec -it some-mysql mysql -u root -p