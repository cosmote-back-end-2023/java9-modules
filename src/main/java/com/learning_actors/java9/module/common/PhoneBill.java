package com.learning_actors.java9.module.common;

import java.math.BigDecimal;
import java.util.Objects;

public final class PhoneBill {

    private final String phoneNumber;
    private final BigDecimal cost;

    public PhoneBill(String phoneNumber, BigDecimal cost) {
        this.phoneNumber = Objects.requireNonNull(phoneNumber, "Phone number must be provided");
        this.cost = Objects.requireNonNull(cost, "Cost must be provided");
    }

    public boolean isNegative() {
        return cost.compareTo(BigDecimal.ZERO) < 0;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public BigDecimal getCost() {
        return cost;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PhoneBill{");
        sb.append("phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", cost=").append(cost);
        sb.append('}');
        return sb.toString();
    }
}
