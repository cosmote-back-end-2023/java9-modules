package com.learning_actors.java9.module.common;

import java.util.Optional;

public class Street {

    private String streetName;
    private String apartmentNumber;
    private Optional<String> apartmentLetter;


    public Street(String streetName, String apartmentNumber) {
        this.streetName = streetName;
        this.apartmentNumber = apartmentNumber;
        this.apartmentLetter = Optional.empty();
    }

    public Street(String streetName, String apartmentNumber, Optional<String> apartmentLetter) {
        this.streetName = streetName;
        this.apartmentNumber = apartmentNumber;
        this.apartmentLetter = apartmentLetter;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public Optional<String> getApartmentLetter() {
        return apartmentLetter;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Street{");
        sb.append("streetName='").append(streetName).append('\'');
        sb.append(", apartmentNumber='").append(apartmentNumber).append('\'');
        sb.append(", apartmentLetter=").append(apartmentLetter);
        sb.append('}');
        return sb.toString();
    }
}
