package com.learning_actors.java9.module.db;

public class MainExample {

    public static void main(String[] args) throws Exception {
        CreateSchemaExample.createSchema();
        CreateTablesExample.createTables();
        InsertExample.createInserts();

    }
}
