package com.learning_actors.java9.module.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTablesExample {

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s?useTimezone=true&serverTimezone=UTC";
    private static final String DB_SCHEMA = "customer_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    // Database credentials, NOT A VALID APPROACH FOR REAL APPLICATIONS
    private static final String USER = "root";
    private static final String PASS = "admin123";

    public static void createTables() throws Exception {
        Class.forName(JDBC_DRIVER);

        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
             Statement stmt = connection.createStatement()) {

            String sql = "create table if not exists CUSTOMER ( " +
                "id SERIAL, " +
                "name VARCHAR(255), " +
                "PRIMARY KEY (id));";

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}