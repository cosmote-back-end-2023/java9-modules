package com.learning_actors.java9.module.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateSchemaExample {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC";
    private static final String DB_SCHEMA = "customer_db";


    // Database credentials, NOT A VALID APPROACH FOR REAL APPLICATIONS
    private static final String USER = "root";
    private static final String PASS = "admin123";


    public static void createSchema() throws Exception{
        Class.forName(JDBC_DRIVER);

        try (Connection connection = DriverManager.getConnection(DB_BASE_URL, USER, PASS);
             Statement stmt = connection.createStatement()) {

            String sql = "create schema if not exists " + DB_SCHEMA + ";";
            stmt.executeUpdate(sql);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
